extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600

const UP = Vector2(0,-1)

var velocity = Vector2()
var maxjump = 1

func get_input():
	velocity.x = 0
	if maxjump<2 and Input.is_action_just_pressed('up'):
		velocity.y = jump_speed
		maxjump+=1
	if is_on_floor():
		maxjump=0
	if Input.is_action_pressed('right'):
		velocity.x += speed
	if Input.is_action_pressed('left'):
		velocity.x -= speed

func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
