extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600
var maxjump = 0

const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	var animation =""
	
	velocity.x = 0
	if is_on_floor():
		maxjump=0
		animation ="stand_right"
		
	if maxjump < 2:
		if Input.is_action_just_pressed('up'):
			animation = "jump"
			velocity.y = jump_speed
			maxjump+=1
			
	if Input.is_action_pressed('right'):
		velocity.x += speed
		$AnimatedSprite.flip_h = false
		animation = "move_right"
	if Input.is_action_pressed('left'):
		velocity.x -= speed
		$AnimatedSprite.flip_h = true
		animation = "move_left"
	if Input.is_action_pressed('down'):
		animation = "crouch"
		
	if Input.is_action_pressed('right') and Input.is_action_pressed('up'):
		$AnimatedSprite.flip_h = false
		animation = "jump_right"
		
	if Input.is_action_pressed('left') and Input.is_action_pressed('up'):
		$AnimatedSprite.flip_h = true
		animation = "jump_right"
		
	if $AnimatedSprite.animation != animation:
		$AnimatedSprite.play(animation)


func _physics_process(delta):
	velocity.y += delta * GRAVITY
	get_input()
	velocity = move_and_slide(velocity, UP)
